package com.example.app.screens

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import com.example.app.R

@Composable
fun Home(navController: NavController) =
    Instruction(
        action = { navController.navigate("index") },
        buttonText = stringResource(R.string.next_button),
        image = painterResource(R.drawable.ic_initial),
        text = stringResource(R.string.home_text),
        title = stringResource(R.string.home_title)
    )