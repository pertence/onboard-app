package com.example.app.screens.tutorials

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import com.example.app.R
import com.example.app.screens.Instruction

@Composable
fun Tutorial1(navController: NavController) {
    Instruction(
        action = { navController.navigate("tutorial2") },
        buttonText = stringResource(R.string.next_button),
        image = painterResource(R.drawable.ic_front_doc),
        text = stringResource(R.string.tutorial_1_text),
        title = stringResource(R.string.tutorial_1_title)
    )
}