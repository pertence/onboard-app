package com.example.app.screens

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import com.example.app.R

@Composable
fun Success(navController: NavController) =
    Instruction(
        buttonText = stringResource(R.string.success_button),
        image = painterResource(R.drawable.ic_initial),
        text = stringResource(R.string.success_text),
        title = stringResource(R.string.success_title),
        action = { navController.navigate("home") }
    )