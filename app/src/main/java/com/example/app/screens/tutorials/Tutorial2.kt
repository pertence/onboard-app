package com.example.app.screens.tutorials

import android.Manifest
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import com.example.app.R
import com.example.app.screens.Instruction
import com.example.app.screens.NavBar

@Composable
fun Tutorial2(navController: NavController) {

    val context = LocalContext.current

    val launcher = rememberLauncherForActivityResult(ActivityResultContracts.RequestPermission()) {}

    val permission = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        NavBar(navController)
        Instruction(
            action = {
                if (permission != PackageManager.PERMISSION_GRANTED) {
                    launcher.launch(Manifest.permission.CAMERA)
                }
                navController.navigate("tutorial3")
            },
            buttonText = stringResource(R.string.authorize_button),
            image = painterResource(R.drawable.ic_access_camera),
            title = stringResource(R.string.tutorial_2_title),
        )
    }
}

/*
@Composable

fun PermissionButton() {
    val context = LocalContext.current

    val launcher = rememberLauncherForActivityResult(ActivityResultContracts.RequestPermission()) {}

    Button(
        onClick = {
            val permission = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
            if (permission != PackageManager.PERMISSION_GRANTED) {
                launcher.launch(Manifest.permission.CAMERA)
            } else {
                Toast.makeText(context, "PERMISSION GRANTED", Toast.LENGTH_SHORT).show()
            }
        }
    ) {
        Text(text = "Button")
    }
}*/