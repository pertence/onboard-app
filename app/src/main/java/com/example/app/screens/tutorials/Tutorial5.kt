package com.example.app.screens.tutorials

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import com.example.app.R
import com.example.app.screens.Instruction
import com.example.app.screens.NavBar

@Composable
fun Tutorial5(navController: NavController) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        NavBar(navController)
        Instruction(
            action = { navController.navigate("tutorial6") },
            buttonText = stringResource(R.string.ack_button),
            image = painterResource(R.drawable.ic_back_stamp),
            title = stringResource(R.string.tutorial_5_title)
        )
    }
}