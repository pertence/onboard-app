package com.example.app.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.app.R

@Composable
fun SelectionRow(action: () -> Unit, image: Painter, text: String) =
    Button(
        onClick = action,
        elevation = ButtonDefaults.elevation(defaultElevation = 0.dp),
        colors = ButtonDefaults.buttonColors(backgroundColor = Color.Transparent)
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Image(
                painter = image,
                contentDescription = "",
                modifier = Modifier
                    .height(100.dp)
                    .width(100.dp)
            )
            Text(text, textAlign = TextAlign.Center, color = Color.White)
        }
    }

@Composable
fun SelectionDoc(navController: NavController) =
    Surface(color = Color.Red) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            Text(
                stringResource(R.string.choose_doc),
                textAlign = TextAlign.Center,
                color = Color.White,
                fontWeight = FontWeight.Bold
            )
            SelectionRow(
                action = { navController.navigate("selection_rg") },
                painterResource(R.drawable.ic_rg_front),
                stringResource(R.string.choose_rg)
            )
            SelectionRow(
                action = { navController.navigate("selection_cnh") },
                painterResource(R.drawable.ic_cnh_front),
                stringResource(R.string.choose_cnh)
            )
        }
    }

@Composable
fun SelectionSide(
    action: () -> Unit,
    text: String,
    imageFront: Painter,
    imageBack: Painter,
    imageFrontBack: Painter
) =
    Surface(color = Color.Red) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            Text(text, color = Color.White)
            SelectionRow(action, imageFront, stringResource(R.string.choose_front))
            SelectionRow(action, imageBack, stringResource(R.string.choose_front))
            SelectionRow(action, imageFrontBack, stringResource(R.string.choose_front_back))
        }
    }

@Composable
fun SelectionRG(navController: NavController) =
    SelectionSide(
        { navController.navigate("photo") },
        stringResource(R.string.choose_rg_side),
        painterResource(R.drawable.ic_rg_front),
        painterResource(R.drawable.ic_rg_back),
        painterResource(R.drawable.ic_rg_front_back)
    )

@Composable
fun SelectionCNH(navController: NavController) =
    SelectionSide(
        { navController.navigate("photo") },
        stringResource(R.string.choose_cnh_side),
        painterResource(R.drawable.ic_cnh_front),
        painterResource(R.drawable.ic_cnh_back),
        painterResource(R.drawable.ic_cnh_front_back)
    )
