package com.example.app.screens.tutorials

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import com.example.app.R
import com.example.app.screens.Instruction
import com.example.app.screens.NavBar

@Composable
fun Tutorial3(navController: NavController) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        NavBar(navController)
        Instruction(
            action = { navController.navigate("tutorial4") },
            buttonText = stringResource(R.string.ack_button),
            image = painterResource(R.drawable.ic_lamp),
            text = stringResource(R.string.tutorial_3_text),
            title = stringResource(R.string.tutorial_3_title)
        )
    }
}