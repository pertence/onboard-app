package com.example.app.screens

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.app.R

@Composable
fun BottomButton(action: () -> Unit, backgroundColor: Color, buttonText: String, textColor: Color) {
    Button(
        colors = ButtonDefaults.buttonColors(backgroundColor = backgroundColor),
        onClick = action,
        modifier = Modifier
            .height(50.dp)
            .fillMaxWidth(),
        shape = RectangleShape
    ) {
        Text(buttonText, color = textColor)
    }
}

