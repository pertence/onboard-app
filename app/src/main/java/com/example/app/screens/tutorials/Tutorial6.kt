package com.example.app.screens.tutorials

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import com.example.app.R
import com.example.app.screens.Instruction
import com.example.app.screens.NavBar

@Composable
fun Tutorial6(navController: NavController) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        NavBar(navController)
        Instruction(
            action = { navController.navigate("tutorial7") },
            buttonText = stringResource(R.string.ack_button),
            image = painterResource(R.drawable.ic_reflex),
            title = stringResource(R.string.tutorial_6_title),
        )
    }
}