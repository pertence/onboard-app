package com.example.app.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun Instruction(
    action: () -> Unit,
    buttonText: String,
    image: Painter,
    text: String = "",
    title: String,
) =
    Surface {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxSize()
        ) {
            Image(
                image,
                contentDescription = "",
                modifier = Modifier
                    .weight(2f)
                    .height(200.dp)
                    .width(200.dp)
            )
            Text(
                title,
                modifier = Modifier.weight(1f),
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center
            )
            Text(text, textAlign = TextAlign.Center, modifier = Modifier.weight(0.5f))
            BottomButton(
                backgroundColor = Color.Red,
                action = action,
                buttonText = buttonText,
                textColor = Color.White
            )
        }
    }
