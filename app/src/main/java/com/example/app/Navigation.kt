package com.example.app

import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.app.screens.*
import com.example.app.screens.tutorials.*

@Composable
fun Navigation() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "home") {
        composable("home") { Home(navController) }
        composable("index") { Index(navController) }
        composable("tutorial1") { Tutorial1(navController) }
        composable("tutorial2") { Tutorial2(navController) }
        composable("tutorial3") { Tutorial3(navController) }
        composable("tutorial4") { Tutorial4(navController) }
        composable("tutorial5") { Tutorial5(navController) }
        composable("tutorial6") { Tutorial6(navController) }
        composable("tutorial7") { Tutorial7(navController) }
        composable("tutorial8") { Tutorial8(navController) }
        composable("tutorial8") { Tutorial8(navController) }
        composable("selection_doc") { SelectionDoc(navController) }
        composable("selection_rg") { SelectionRG(navController) }
        composable("selection_cnh") { SelectionCNH(navController) }
        composable("photo") { Photo(navController) }
        composable("success") { Success(navController) }
    }
}


