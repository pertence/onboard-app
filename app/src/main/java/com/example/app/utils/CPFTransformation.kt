package com.example.app.utils

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.input.VisualTransformation

class CPFTransformation : VisualTransformation {
    override fun filter(text: AnnotatedString): TransformedText {

        val offsetTranslator = object : OffsetMapping {
            override fun originalToTransformed(offset: Int) = offset + (offset / 3)
            override fun transformedToOriginal(offset: Int) = offset - (offset / 3)
        }

        return TransformedText(
            AnnotatedString(text.text.replace("""\d\d\d""".toRegex(), "$0.")),
            offsetTranslator
        )
    }


}

/*
val regexFind = """(\d{3})(\d{3})(\d{3})(\d{2})"""

val regexReplace = """$0.$1.$2-$3"""

val out = Regex(regexFind).replace(trimmed, regexReplace)
*/