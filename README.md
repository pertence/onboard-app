# Onboard

## Pre requirements

This project requires Android Studio Arctic Fox Beta 5 or newer to run and build. 

## Tools

This project uses the following components of Android SDK:
- Jetpack Compose: declarative ui toolkit
- Android CameraX: camera library

## Limitations

This project has some limitation because of:
- Missing assets
- Some assets are available only in bitmap 
- Graphic design is available only as images
- No specification of font size
- No specification of component dimensions

## Future improvements

The project could be improved by adding:
- CPF validation
- Photo saving

